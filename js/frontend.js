var initPhotoSwipeFromDOM = function(gallerySelector) {

    // parse slide data (url, title, size ...) from DOM elements 
    // (children of gallerySelector)
    var parseThumbnailElements = function(el) {
        var thumbElements = el.childNodes,
            numNodes = thumbElements.length,
            items = [],
            figureEl,
            linkEl,
            size,
            item;

        for(var i = 0; i < numNodes; i++) {

            figureEl = thumbElements[i]; // <figure> element

            // include only element nodes 
            if(figureEl.nodeType !== 1) {
                continue;
            }

            linkEl = $(figureEl).find('a').get(0);//figureEl.children[0]; // <a> element

            if (linkEl) {


                size = linkEl.getAttribute('data-size').split('x');

                // create slide object
                item = {
                    src: linkEl.getAttribute('href'),
                    w: parseInt(size[0], 10), //window.screen.availWidth*0.333
                    h: parseInt(size[1], 10)
                };



                if(figureEl.children.length > 1) {
                    // <figcaption> content
                    item.title = figureEl.children[1].innerHTML; 
                }

                if(linkEl.children.length > 0) {
                    // <img> thumbnail element, retrieving thumbnail url
                    item.msrc = linkEl.children[0].getAttribute('src');
                } 

                item.el = figureEl; // save link to element for getThumbBoundsFn
                items.push(item);

            }
        }

        return items;
    };

    // find nearest parent element
    var closest = function closest(el, fn) {
        return el && ( fn(el) ? el : closest(el.parentNode, fn) );
    };

    // triggers when user clicks on thumbnail
    var onThumbnailsClick = function(e) {
        e = e || window.event;
        e.preventDefault ? e.preventDefault() : e.returnValue = false;

        var eTarget = e.target || e.srcElement;

        // find root element of slide
        var clickedListItem = closest(eTarget, function(el) {
            //return (el.tagName && el.tagName.toUpperCase() === 'FIGURE');
            return (el.tagName && el.tagName.toUpperCase() === 'DIV');
        });

        if(!clickedListItem) {
            return;
        }

        // find index of clicked item by looping through all child nodes
        // alternatively, you may define index via data- attribute
        var clickedGallery = $(clickedListItem).closest('.swiper-slide').get(0),
            //clickedGallery = clickedListItem.parentNode,
            childNodes = clickedListItem.parentNode.childNodes,
            numChildNodes = childNodes.length,
            nodeIndex = 0,
            index;

        for (var i = 0; i < numChildNodes; i++) {
            if(childNodes[i].nodeType !== 1) { 
                continue; 
            }

            if(childNodes[i] === clickedListItem) {
                index = nodeIndex;
                break;
            }
            nodeIndex++;
        }


        index = $(clickedListItem).closest('.gallery-item').index();

        if(index >= 0) {
            // open PhotoSwipe if valid index found
            openPhotoSwipe( index, clickedGallery );
        }
        return false;
    };

    // parse picture index and gallery index from URL (#&pid=1&gid=2)
    var photoswipeParseHash = function() {
        var hash = window.location.hash.substring(1),
        params = {};

        if(hash.length < 5) {
            return params;
        }

        var vars = hash.split('&');
        for (var i = 0; i < vars.length; i++) {
            if(!vars[i]) {
                continue;
            }
            var pair = vars[i].split('=');  
            if(pair.length < 2) {
                continue;
            }           
            params[pair[0]] = pair[1];
        }

        if(params.gid) {
            params.gid = parseInt(params.gid, 10);
        }

        return params;
    };

    var openPhotoSwipe = function(index, galleryElement, disableAnimation, fromURL) {
        var pswpElement = document.querySelectorAll('.pswp')[0],
            gallery,
            options,
            items;

        items = parseThumbnailElements(galleryElement);

        // define options (if needed)
        options = {

            // define gallery index (for URL)
            galleryUID: galleryElement.getAttribute('data-pswp-uid'),

            getThumbBoundsFn: function(index) {
                // See Options -> getThumbBoundsFn section of documentation for more info
                var thumbnail = items[index].el.getElementsByTagName('a')[0], // find thumbnail
                    pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
                    rect = thumbnail.getBoundingClientRect(); 

                return {x:rect.left, y:rect.top + pageYScroll, w:rect.width};
            }

        };

        // PhotoSwipe opened from URL
        if(fromURL) {
            if(options.galleryPIDs) {
                // parse real index when custom PIDs are used 
                // http://photoswipe.com/documentation/faq.html#custom-pid-in-url
                for(var j = 0; j < items.length; j++) {
                    if(items[j].pid == index) {
                        options.index = j;
                        break;
                    }
                }
            } else {
                // in URL indexes start from 1
                options.index = parseInt(index, 10) - 1;
            }
        } else {
            options.index = parseInt(index, 10);
        }

        // exit if index not found
        if( isNaN(options.index) ) {
            return;
        }

        if(disableAnimation) {
            options.showAnimationDuration = 0;
        }

        // Pass data to PhotoSwipe and initialize it
        gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
        gallery.init();
    };

    // loop through all gallery elements and bind events
    var galleryElements = document.querySelectorAll( gallerySelector );

    for(var i = 0, l = galleryElements.length; i < l; i++) {
        galleryElements[i].setAttribute('data-pswp-uid', i+1);
        galleryElements[i].onclick = onThumbnailsClick;
    }

    // Parse URL and open gallery if it contains #&pid=3&gid=1
    var hashData = photoswipeParseHash();
    if(hashData.pid && hashData.gid) {
        openPhotoSwipe( hashData.pid ,  galleryElements[ hashData.gid - 1 ], true, true );
    }
};


//Swiper js
if( $(window).width() > 768) {
    var mySwiper = new Swiper ('.swiper-container', {
        // Optional parameters
        loop: true,

        // Navigation arrows
        navigation: {
          nextEl: '.gallery-right',
          prevEl: '.gallery-left',
        },
      });
}
else {
    var mySwiper = new Swiper ('.swiper2', {
        // Optional parameters
        spaceBetween: 60,
        slidesPerView: 'auto',
        freeMode: true,
        freeModeMomentum: true,
        setWrapperSize: true,

      });
    var mySwiper = new Swiper ('.swiper1', {
        // Optional parameters
        slidesPerView: 'auto',
        freeMode: true,
        setWrapperSize: true,

        navigation: {
          nextEl: '.gallery-right',
        },
      });
}

//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
var num = 1;
var sticky;

var setLayouts = debounce(function() {
	// do stuff
}, 250);
/*
document.addEventListener('touchmove', function (e) { e.preventDefault(); }, isPassive() ? {
	capture: false,
	passive: false
} : false);*/

$(function(){
	FastClick.attach(document.body);
	initPhotoSwipeFromDOM('.swiper-slide');
   
    $('.reservation-calendar').dateRangePicker({
        inline:true,
        container: '.reservation-calendar',
        alwaysOpen:true,
        startOfWeek: 'monday',
        singleMonth: false,
        language: 'lt'
    });
    
    if ($(".stick-side").length && screen.width>768) {
         sticky = new StickySidebar('.stick-side', {
            topSpacing: $('header').outerHeight(),
            containerSelector: '.reservation-items',
            innerWrapperSelector: '.reservation-form'
        });
    }
});

$(window).resize(setLayouts);

// debounce
function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	}
}

// ref https://github.com/WICG/EventListenerOptions/pull/30
function isPassive() {
    var supportsPassiveOption = false;
    try {
        addEventListener("test", null, Object.defineProperty({}, 'passive', {
            get: function () {
                supportsPassiveOption = true;
            }
        }));
    } catch(e) {}
    return supportsPassiveOption;
}


$(document)
.on('click', '.mobile-menu', function() {
    $('body').addClass('menu-opened');
})
.on('click', '.mobile-close', function() {
    $('body').removeClass('menu-opened');
})
.on('click','.lang-list li', function() {
    if( screen.width<769 ) {
        $('.lang-list').css('opacity', '0');
    }
})
.on('click', '.reserve-button, .hero-button', function() {
    $('.dark-bg').fadeIn(300);
    $('.reg-popup').fadeIn(400);
    $('.before-selection').hide();
    $('.reservation-dates').show();
    $('.reservation-selected').show();
    $('.form-bottom-text').show();
    $('.form-button').removeClass('button-deactivated');
    $('.wrap').addClass('reservation-fix');
})
.on('click', '.reg-decline', function() {
    $('.reg-popup').fadeOut(400);
    $('.dark-bg').fadeOut(400);
})
.on('click', 'div.reservation-top', function() {
    if( $(this).next().css('display') == 'block') {
        $('.reservation-button', this).hide();
        $('.reservation-button2', this).show();
    }
    else {
        $('.reservation-button', this).show();
        $('.reservation-button2', this).hide();
    }
	$(this).next().slideToggle(200);
    setTimeout(function(){
        sticky.updateSticky();
    }, 300);
})
.on('click', '.reservation-option', function() {
	if ($(this).hasClass('active')) {
		$(this).removeClass('active');
	}
	else $(this).addClass('active');
})
.on('click', '.hero-down', function() {
	$('html, body').animate({
    scrollTop: $(".scroll").offset().top
    }, 800);
});




